package com.bootcamp.android.earthquakeraport.Database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class EarthquakeContract {
    public EarthquakeContract() {}

    public static final String CONTENT_AUTHORITY = "com.bootcamp.android.earthquakeraport";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_EARTHQUAKE = "earthquake";

    public static final class EarthquakeEntry implements BaseColumns{

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_EARTHQUAKE);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EARTHQUAKE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EARTHQUAKE;

        public final static String TABLE_NAME = "earthquake_table";
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_EARTHQUAKE_LOCATION ="location";
        public final static String COLUMN_EARTHQUAKE_DATE ="date";
        public final static String COLUMN_EARTHQUAKE_MAGNITUDE = "magnetude";
    }
}

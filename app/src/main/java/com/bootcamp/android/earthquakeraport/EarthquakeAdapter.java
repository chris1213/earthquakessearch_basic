package com.bootcamp.android.earthquakeraport;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class EarthquakeAdapter extends ArrayAdapter<Earthquake> implements Filterable {

    public static final String LOCATION_SEPARATOR = " of ";

    private boolean guardian = false;
    private ArrayList<Earthquake> orginalList;
    private String primaryLocation;
    private String locationOffset;



    public EarthquakeAdapter(Context context, List<Earthquake> earthquakes) {
        super(context, 0, earthquakes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.earthquake_list_content, parent, false);
        }

        TextView magnitudeView = (TextView) listItemView.findViewById(R.id.magnitude);

        Earthquake currentEarthquake = getItem(position);
        String formattedMagnitude = formatMagnitude(currentEarthquake.getMagnitude());
        magnitudeView.setText(formattedMagnitude);

        String originalLocation = currentEarthquake.getLocation();

        if (originalLocation.contains(LOCATION_SEPARATOR)) {
            String[] parts = originalLocation.split(LOCATION_SEPARATOR);
            locationOffset = parts[0] + LOCATION_SEPARATOR;
            primaryLocation = parts[1];
        } else {
            locationOffset = getContext().getString(R.string.near_the);
            primaryLocation = originalLocation;
        }

        TextView primaryLocationView = (TextView) listItemView.findViewById(R.id.primary_location);
        primaryLocationView.setText(primaryLocation);

        TextView locationOffsetView = (TextView) listItemView.findViewById(R.id.location_offset);
        locationOffsetView.setText(locationOffset);

        Date dateObject = new Date(currentEarthquake.getTimeInMilliseconds());

        TextView dateView = (TextView) listItemView.findViewById(R.id.date);
        String formattedDate = formatDate(dateObject);
        dateView.setText(formattedDate);

        TextView timeView = (TextView) listItemView.findViewById(R.id.time);
        String formattedTime = formatTime(dateObject);
        timeView.setText(formattedTime);

        return listItemView;
    }

    @Override
    public void addAll(@NonNull Collection<? extends Earthquake> collection) {
        super.addAll(collection);
        if (collection != null && !guardian) {
            orginalList = (ArrayList<Earthquake>) collection;
            guardian = true;
        } else {
            Log.i("add All Error", "null");
        }
    }
    @NonNull
    @Override
    public Filter getFilter() {
        return new EarthquakeFilter();
    }

    private class EarthquakeFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            charSequence = charSequence.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if (charSequence != null && charSequence.length() > 0) {
                ArrayList<Earthquake> filteredList = new ArrayList<>();
                for (int i = 0; i < orginalList.size(); i++) {
                    Earthquake currentItem = orginalList.get(i);
                    if (currentItem.getLocation().toLowerCase().contains(charSequence)) {
                        filteredList.add(currentItem);
                    }
                }
                results.values = filteredList;
                results.count = filteredList.size();

                return results;
            } else {
                results.values = orginalList;
                results.count = orginalList.size();
                return results;
            }

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            notifyDataSetChanged();
            clear();
            addAll((ArrayList<Earthquake>) filterResults.values);
            notifyDataSetInvalidated();
        }

    }

    private String formatMagnitude(double magnitude) {
        DecimalFormat magnitudeFormat = new DecimalFormat("0.0");
        return magnitudeFormat.format(magnitude);
    }

    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }

    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }
}


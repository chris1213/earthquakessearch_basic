package com.bootcamp.android.earthquakeraport;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SettingsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
    }

    public static class EarthquakePreferenceFragment extends PreferenceFragment
            implements Preference.OnPreferenceChangeListener,
            DatePickerDialog.OnDateSetListener {

        private Preference startDate;
        private Preference endDate;
        private static boolean clickedDialogStart;


        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_main);

            Preference minMagnitude = findPreference(getString(R.string.settings_min_magnitude_key));
            bindPreferenceSummaryToValue(minMagnitude);

            Preference minElements = findPreference(getString(R.string.settings_min_elements_key));
            bindPreferenceSummaryToValue(minElements);

            Preference orderBy = findPreference(getString(R.string.settings_order_by_key));
            bindPreferenceSummaryToValue(orderBy);

            startDate = findPreference(getString(R.string.date_picker_start_key));
            endDate = findPreference(getString(R.string.date_picker_end_key));


            startDate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    clickedDialogStart = true;
                    showDateDialog();
                    return false;
                }
            });

            endDate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    clickedDialogStart = false;
                    showDateDialog();
                    return false;
                }
            });
        }


        private void saveStartDate(String date) {
            bindPreferenceSummaryToValue_v2(startDate, date);
        }

        private void saveEndDate(String date) {
            bindPreferenceSummaryToValue_v2(endDate, date);
        }


        private void bindPreferenceSummaryToValue(Preference preference) {
            preference.setOnPreferenceChangeListener(this);
            SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext());

            String preferenceString = preferences.getString(preference.getKey(), "");
            onPreferenceChange(preference, preferenceString);
        }

        private void bindPreferenceSummaryToValue_v2(Preference preference, String startDate) {
            preference.setOnPreferenceChangeListener(this);
            SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext());

            SharedPreferences.Editor editor;
            editor = preferences.edit();
            editor.putString(preference.getKey(), startDate);
            editor.commit();
            onPreferenceChange(preference, startDate);
        }


        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {

            String stringValue = value.toString();
            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int prefIndex = listPreference.findIndexOfValue(stringValue);
                if (prefIndex >= 0) {
                    CharSequence[] labels = listPreference.getEntries();
                    preference.setSummary(labels[prefIndex]);
                }
            }
            else if(preference instanceof EditTextPreference){
                preference.setSummary(stringValue);
            }
            else {
                preference.setSummary(stringValue);
            }
            return true;
        }


        @TargetApi(Build.VERSION_CODES.M)
        private void showDateDialog() {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    this, year, month, day);
            datePickerDialog.show();

        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {

            onDataSetDetail(datePicker, year, month, day);
        }

        private void onDataSetDetail(DatePicker datePicker, int year, int month, int day) {
            month = month + 1;
            String yearS = String.valueOf(year);
            String monthS = String.valueOf(month);
            String dayS = String.valueOf(day);

            if (month < 10) {
                monthS = "0" + String.valueOf(month);
            }
            if (day < 10) {
                dayS = "0" + String.valueOf(day);
            }

            String dateStart = yearS + "/" + monthS + "/" + dayS;
            Date date = stringToDate(dateStart);

            String odpData = formatDate(date);

            if (clickedDialogStart)
                saveStartDate(odpData);
            else
                saveEndDate(odpData);
        }


        private Date stringToDate(String dateStart) {
            String myDate = dateStart;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Date date = null;
            try {
                date = sdf.parse(myDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date;
        }


        private String formatDate(Date dateObject) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(dateObject);
        }

    }
}

package com.bootcamp.android.earthquakeraport;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.bootcamp.android.earthquakeraport.Favorite.Favorite;

import java.util.ArrayList;
import java.util.List;

public class EarthquakeListActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<List<Earthquake>>,
        SharedPreferences.OnSharedPreferenceChangeListener {


    private EditText edit_search;
    private ArrayList<Earthquake> earthquakeArrayList;
    private static final int EARTHQUAKE_LOADER_ID = 1;
    private EarthquakeAdapter mAdapter;
    private boolean mTwoPane;
    private static final String USGS_REQUEST_URL =
            "https://earthquake.usgs.gov/fdsnws/event/1/query";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthquake_list);

        edit_search = (EditText)findViewById(R.id.edit_search);
        edit_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                System.out.println("Text ["+s+"]");
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());


        ListView recyclerView = (ListView) findViewById(R.id.earthquake_list);
        assert recyclerView != null;

        mAdapter = new EarthquakeAdapter(this, new ArrayList<Earthquake>());
        recyclerView.setAdapter(mAdapter);

        if (findViewById(R.id.earthquake_detail_container) != null) {
            mTwoPane = true;
        }


        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                final Earthquake item = earthquakeArrayList.get(position);
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("KEY", item);

                    EarthquakeDetailFragment fragment = new EarthquakeDetailFragment();
                    fragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.earthquake_detail_container, fragment)
                            .commit();
                } else {
                    Intent intent = new Intent(EarthquakeListActivity.this, EarthquakeDetailActivity.class);
                    intent.putExtra("KLUCZ", item);
                    startActivity(intent);
                }
            }
        });


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            LoaderManager loaderManager = getLoaderManager();
            loaderManager.initLoader(EARTHQUAKE_LOADER_ID, null, this);
        }
    }

    @Override
    public Loader<List<Earthquake>> onCreateLoader(int id, Bundle args) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String minMagnitude = sharedPrefs.getString(
                getString(R.string.settings_min_magnitude_key),
                getString(R.string.settings_min_magnitude_default));

        String orderBy = sharedPrefs.getString(
                getString(R.string.settings_order_by_key),
                getString(R.string.settings_order_by_default));

        String minElements = sharedPrefs.getString(
                getString(R.string.settings_min_elements_key),
                getString(R.string.settings_min_elements_default));

        String dateStart = sharedPrefs.getString(
                getString(R.string.date_picker_start_key),
                getString(R.string.date_picker_start_default));

        String dateEnd = sharedPrefs.getString(
                getString(R.string.date_picker_end_key),
                getString(R.string.date_picker_end_default));


        Uri baseUri = Uri.parse(USGS_REQUEST_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("format", "geojson");
        uriBuilder.appendQueryParameter("limit", minElements);
        uriBuilder.appendQueryParameter("minmag", minMagnitude);
        uriBuilder.appendQueryParameter("orderby", orderBy);
        uriBuilder.appendQueryParameter("starttime", dateStart);
        uriBuilder.appendQueryParameter("endtime", dateEnd);

        return new EarthquakeLoader(this, uriBuilder.toString());
    }

    @Override
    public void onLoadFinished(Loader<List<Earthquake>> loader, List<Earthquake> earthquakes) {
        mAdapter.clear();

        if (earthquakes != null && !earthquakes.isEmpty()) {
            earthquakeArrayList = new ArrayList<>();
            earthquakeArrayList = (ArrayList<Earthquake>) earthquakes;
            mAdapter.addAll(earthquakes);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Earthquake>> loader) {
        mAdapter.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(EarthquakeListActivity.this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        } else if (id == R.id.goToFavorite){
            Intent settingsIntent = new Intent(EarthquakeListActivity.this, Favorite.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(getString(R.string.settings_min_magnitude_key)) ||
                key.equals(getString(R.string.settings_order_by_key)) ||
                key.equals(getString(R.string.settings_min_elements_key)) ||
                key.equals(getString(R.string.date_picker_start_key)) ||
                key.equals(getString(R.string.date_picker_end_key))) {
            mAdapter.clear();

            getLoaderManager().restartLoader(EARTHQUAKE_LOADER_ID, null, this);
        }
    }

}
